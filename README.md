Un club tiene que administrar el cobro de cuotas a sus socios los cuales se clasifican además de
los comunes en vitalicios y adherentes. Los vitalicios son aquellos que registran más de 20 años
de antigüedad como socio común. Los socios adherentes son aquellos que pagan una cuota mensual que difiere del socio común en un 50% menos. Los datos que se requieren son Apellido,
nombre, documento. Los socios vitalicios no pagan cuota, los socios comunes pagan 1500 pesos.
Cuando se realiza el pago de cuotas se genera un comprobante de pago en donde se indican los
datos del socio, el importe de las cuotas, y las cuotas pagadas. Para los casos de los vitalicios se
emite un comprobante de mensual que es válido como pago con solo presentar su documento


NOTA: Los archivos de este repositorio fueron transferidos de un repositorio anterior https://gitlab.com/Gustavocon/tp-n-2-situacion-2

/*
Situación 2
Un club tiene que administrar el cobro de cuotas a sus socios los cuales se clasifican además de
los comunes en vitalicios y adherentes. Los vitalicios son aquellos que registran más de 20 años
de antigüedad como socio común. Los socios adherentes son aquellos que pagan una cuota mensual que difiere del socio común en un 50% menos. Los datos que se requieren son Apellido,
nombre, documento. Los socios vitalicios no pagan cuota, los socios comunes pagan 1500 pesos.
Cuando se realiza el pago de cuotas se genera un comprobante de pago en donde se indican los
datos del socio, el importe de las cuotas, y las cuotas pagadas. Para los casos de los vitalicios se
emite un comprobante de mensual que es válido como pago con solo presentar su documento
*/

package situacion;

public class App { //Metodo principal, utilizado para ejecutar el codigo

    public static void main(String[] args) {
        Socio socio = new Socio();          // Se declara una instancia de la clase Socio y 
        socio.setNombre("Gustavo","Contreras"); //se inicializan sus variables
        socio.setDNI(43995185);
        socio.setAniosAntiguedad(19);           // Puede cambiarse el valor de años de antiguedad o
        socio.setMesesAntiguedad(3);            // el valor de Adherente, para observar las distintas
        socio.setAdherente(false);              // situaciones

        Club club = new Club();
        if (socio.getAniosAntiguedad() > 20)
        {
            if (club.generarComprobanteMensual(socio) == true)
            {
                System.out.println("\nComprobrante generado exitosamente!");
            }
        }
        else
        {
            if (club.generarComprobantePago(socio) == true)
            {
                System.out.println("\nPago realizado correctamente!");
            }
        }

    }
}

package situacion;

public class Socio 
{
    // Atributos
    private String apellido;
    private String nombre;
    private Integer dni;
    private Integer aniosAntiguedad;
    private Integer mesesAntiguedad;
    private boolean adherente;

    //Metodos "setters". Utilizados para inicializar variables
    public void setNombre(String nombre, String apellido)
    {
        this.nombre = nombre;
        this.apellido = apellido;
    }
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }
    public void setDNI (Integer dni)
    {
        this.dni = dni;
    }
    public void setAniosAntiguedad(Integer aniosAntiguedad)
    {
        this.aniosAntiguedad = aniosAntiguedad;
    }
    public void setMesesAntiguedad(Integer mesesAntiguedad)
    {
        this.mesesAntiguedad = mesesAntiguedad;
    }
    public void setAdherente(boolean adherente)
    {
        this.adherente = adherente;
    }

    //Metodos "getter". Utilizados para obtener el valor de las variables
    public String getNombre()
    {
        return this.nombre;
    }
    public String getApellido()
    {
        return this.apellido;
    }
    public Integer getDNI()
    {
        return this.dni;
    }
    public Integer getAniosAntiguedad()
    {
        return this.aniosAntiguedad;
    }
    public Integer getMesesAntiguedad()
    {
        return this.mesesAntiguedad;
    }
    public boolean getAdherente()
    {
        return this.adherente;
    }
}
